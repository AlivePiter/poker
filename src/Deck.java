import oblig1.Card;

import java.util.*;
import java.util.stream.Collectors;

public class Deck {

    private static ArrayList<Card> deck = new ArrayList<Card>();

    /**
     * Creates a normal deck with 52 card and 13 cards of each suit.
     */
    public void createDeck(){
        // ArrayList<Card> deck = new ArrayList<Card>();

        for(int i = 1; i<14; i++)
        {
            deck.add(new Card('S', i));
            deck.add(new Card('D', i));
            deck.add(new Card('H', i));
            deck.add(new Card('C', i));
        }

    }

    /**
     * Shuffles a deck and lets you use i amount of cards from the deck.
     * @param i the amount of cards you want to use.
     * @return shuffledDeck The a deck that is shuffled.
     */
    public ArrayList<Card> assign(int i)
    {
        Collections.shuffle(deck);
        ArrayList<Card> shuffledDeck = new ArrayList<>();
        for(int n = 1; n <= i; n++)
        {
            shuffledDeck.add(deck.get(n));
        }
    return shuffledDeck;

    }

    public static void main(String[] args)
    {
        Deck deck = new Deck();
        deck.createDeck();

        {
            //Prints out each card of suit 'S'.
            deck.assign(6).stream().filter(c -> (c.getSuit() == 'S')).forEach(c -> System.out.println(c.getDetails()));
            System.out.println("------------------------------");


        }


        {
            //Collects all cards with suit 'S' to a new collection.
            List<Card> result = deck.assign(6).stream().filter(h -> h.getSuit() == 'H').collect(Collectors.toList());
            System.out.println("------------------------------");

        }


        {
            //
            List<Character> collect = deck.assign(6).stream().map(Card::getSuit).collect(Collectors.toList());
            System.out.println(collect);
            System.out.println("------------------------------");
        }


        {
            //Adds the values of all cards to a sum, and prints the sum.
            int sum = deck.assign(6).stream().reduce(0, (subtotal, card) -> subtotal + card.getFace(), Integer::sum);
            System.out.println(sum);
            System.out.println("------------------------------");
        }


        {
            //Checks if any of your i amount of cards contains a card with suit 'D'. Return true if it does, false if not.
            boolean result = deck.assign(6).stream().anyMatch(c -> (c.getSuit() == 'D'));
            System.out.println(result);
            System.out.println("------------------------------");
        }


        {
            //Checks if you have a poker flush, return true if you have, false if not.
            Map<Character, Long> count = deck.assign(5)
                    .stream()
                    .collect(Collectors.groupingBy(Card::getSuit, Collectors.counting()));

            System.out.println(count.values().stream().anyMatch(i -> i >= 5L));


        }
    }





}
